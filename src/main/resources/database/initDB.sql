create table movies
(
    movie_id serial primary key not null,
    movie_name character varying(100) not null unique,
    movie_price integer not null,
    movie_rate character varying(100) not null,
    movie_start timestamp without time zone not null
);