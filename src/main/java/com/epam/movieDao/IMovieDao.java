package com.epam.movieDao;

import com.epam.model.MovieEvent;

import java.util.List;

public interface IMovieDao {
    List<MovieEvent> getAllMovies();
    MovieEvent getMovieById(Integer id);
    void addMovie(MovieEvent movieEvent);
    void deleteMovie(Integer id);
}
