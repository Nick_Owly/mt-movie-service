package com.epam.movieDao;

import com.epam.model.MovieEvent;
import com.epam.model.Rating;
import com.epam.utils.MovieRowMapper;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovieDaoImpl implements IMovieDao{

    private static final String SQL_GET_ALL_MOVIES = "SELECT movie_id, movie_name, movie_price," +
            "movie_rate, movie_start FROM movies";

    private static final String SQL_GET_MOVIE_BY_ID = "SELECT movie_id, movie_name, movie_price," +
            "movie_rate, movie_start FROM movies WHERE movie_id = ?";

    private static final String SQL_ADD_MOVIE = "INSERT INTO movies(movie_id, movie_name, movie_price," +
            "movie_rate, movie_start) VALUE (?,?,?,?)";

    private final JdbcTemplate jdbcTemplate;

    public MovieDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<MovieEvent> getAllMovies() {
        RowMapper<MovieEvent> rowMapper = new MovieRowMapper();
        return this.jdbcTemplate.query(SQL_GET_ALL_MOVIES,rowMapper);
    }

    @Override
    public MovieEvent getMovieById(Integer id) {
        RowMapper rowMapper = new MovieRowMapper();
        MovieEvent movieEvent = (MovieEvent) jdbcTemplate.queryForObject(SQL_GET_MOVIE_BY_ID,rowMapper,id);
        return movieEvent;
    }

    @Override
    public void addMovie(MovieEvent movieEvent) {

        jdbcTemplate.update(SQL_ADD_MOVIE,movieEvent.getId(),movieEvent.getMovie_name(),
                movieEvent.getMovie_rate(),movieEvent.getMovie_start());
    }

    @Override
    public void deleteMovie(Integer id) {
/          // to do
    }
}
