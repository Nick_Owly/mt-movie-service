package com.epam.utils;

import com.epam.model.MovieEvent;
import com.epam.model.Rating;
import lombok.Builder;

import java.time.LocalDateTime;

public class MovieBuilder {

    @Builder(builderMethodName = "builder")
    public static MovieEvent newMovieEvent(Integer id, String name, int price, Rating rate,
                                           LocalDateTime start){
        return new MovieEvent(id,name,price,rate,start);
    }
}
