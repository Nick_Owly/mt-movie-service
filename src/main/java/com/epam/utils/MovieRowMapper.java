package com.epam.utils;

import com.epam.model.MovieEvent;
import com.epam.model.Rating;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieRowMapper implements RowMapper<MovieEvent> {

    @Override
    public MovieEvent mapRow(ResultSet rs, int rowNum) throws SQLException {
        MovieEvent event = MovieBuilder.builder()
                .id(rs.getInt("movie_id"))
                .name(rs.getString("movie_name"))
                .price(rs.getInt("movie_price"))
                .rate(Rating.getByName(rs.getString("movie_rate")))
                .start(rs.getTimestamp("movie_start").toLocalDateTime())
                .build();

        return event;
    }
}
