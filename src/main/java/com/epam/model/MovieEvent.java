package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Value
public final class MovieEvent {
    @NotNull
    @Min(1)
    private Integer id;
    @NotEmpty(message="Movie title must not be empty")
    private String movie_name;
    @NotNull(message="Please, enter the price")
    private int movie_price;
    @NotNull(message="Please, identify the rating")
    private Rating movie_rate;
    @NotNull(message="Please, specify the date of movie show")
    private LocalDateTime movie_start;
}
