package com.epam.model;

import com.epam.exceptions.RateNotFoundException;

public enum Rating {

    LOW("LOW"),
    MID("MID"),
    HIGH("HIGH");

    private String name;

    Rating(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Rating getByName(String name){
        for(Rating rating: values()){
            if(rating.getName().equalsIgnoreCase(name)){
                return rating;
            }
        }
        throw new RateNotFoundException("Unknown rate type: "+name);
    }
}
